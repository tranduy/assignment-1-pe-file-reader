
/// @file 
/// @brief Main application file

#include <stdio.h>

#include "define_pe.h"
/// Application name string
#define APP_NAME "section-extractor"

/// @brief Print usage test
/// @param[in] f File to print to (e.g., stdout)
void usage(FILE *f)
{
  fprintf(f, "Usage: " APP_NAME " <in_file> <section_name> <out_file>\n");
}

/// @brief Application entry point
/// @param[in] argc Number of command line arguments
/// @param[in] argv Command line arguments
/// @return 0 in case of success or error code
int main(int argc, char** argv)
{
  (void) argc; (void) argv; // supress 'unused parameters' warning
  if (argc != 4) {
        return 1;
    }
  FILE *fileIn; 
  FILE *fileOut;
  char *sectionName = argv[2];

  if ((fileIn = fopen(argv[1], "rb")) == NULL) return 1;
  if ((fileOut = fopen(argv[3], "wb")) == NULL) return 1;

  struct PEFile *peFile = malloc(sizeof(struct PEFile));

  readFile(fileIn, peFile);
  printSection(fileIn, fileOut, peFile, sectionName);

  free(peFile->sectionHeaders);
  free(peFile);
  fclose(fileIn);
  fclose(fileOut);

  return 0;
}
