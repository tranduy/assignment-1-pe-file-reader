/// @file 
/// @brief Contains PE functions realisations

#include <string.h>

#include "define_pe.h"


/** 
 * @brief Reading PE file 
 * @param fileIn PE file 
 * @param peFile PEFile structure
*/
void readFile(FILE *fileIn, struct PEFile *peFile){
    fseek(fileIn, MAIN_OFFSET, SEEK_SET);
    
    uint32_t effectiveEffset;
    int oneChar =1;
    fread(&effectiveEffset, sizeof(effectiveEffset), oneChar, fileIn);
    fseek(fileIn, (uint32_t) effectiveEffset, SEEK_SET);  

    fread(&peFile->magic, sizeof(peFile->magic), oneChar, fileIn);
    fread(&peFile->header, sizeof(peFile->header), oneChar, fileIn);

    peFile->sectionHeaders = malloc(peFile->header.NumberOfSections * sizeof(struct SectionHeader));

    fseek(fileIn, peFile->header.SizeOfOptionalHeader, SEEK_CUR);

    fread(peFile->sectionHeaders, sizeof(struct SectionHeader), peFile->header.NumberOfSections, fileIn);
}

/** 
 * @brief Prints section if name matched
 * @param fileIn PE file 
 * @param fileOut File to write section
 * @param peFile PEFile structure
 * @param section Name of section to print
*/
void printSection(FILE *fileIn, FILE *fileOut, struct PEFile *peFile, char *section){
    for (size_t i = 0; i < peFile->header.NumberOfSections; i++){

        if ( strcmp((char*)peFile->sectionHeaders[i].Name, section) == 0){

            fseek(fileIn, peFile->sectionHeaders[i].PointerToRawData, SEEK_SET);
            char *rawData = malloc(peFile->sectionHeaders[i].SizeOfRawData);
            fread(rawData, peFile->sectionHeaders[i].SizeOfRawData, 1, fileIn);
            fwrite(rawData, peFile->sectionHeaders[i].SizeOfRawData, 1, fileOut);
            free(rawData);
        }
        
    }

}
