/// @file
/// @brief Contains PE structures and functions definitions
#pragma once

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
/// @brief  Offset to the location of pointer to PE header in PE file
#define MAIN_OFFSET 0x3c

/**
 * @brief Stores PE header data
 */
#if defined _MSC_VER
#pragma pack(push, 1)
#endif
struct
#if defined __clang__ || __GNUC__
    __attribute__((packed))
#endif
    PEHeader
{
    /// @brief The number that identifies the type of target machine
    uint16_t Machine;
    /// @brief The number of sections. This indicates the size of the section table, which immediately follows the headers
    uint16_t NumberOfSections;
    /// @brief The low 32 bits of the number of seconds since 00:00 January 1, which indicates when the file was created
    uint32_t TimeDateStamp;
    /// @brief The file offset of the COFF symbol table, or zero if no COFF symbol table is present
    uint32_t PointerToSymbolTable;
    /// @brief The number of entries in the symbol table
    uint32_t NumberOfSymbols;
    /// @brief The size of the optional header
    uint16_t SizeOfOptionalHeader;
    /// @brief The flags that indicate the attributes of the file
    uint16_t Characteristics;
};
#if defined _MSC_VER
#pragma pack(pop)
#endif

/**
 * @brief Stores section header data
 */
#if defined _MSC_VER
#pragma pack(push, 1)
#endif
struct
#if defined __clang__ || __GNUC__
    __attribute__((packed))
#endif
    SectionHeader
{
    uint8_t Name[8];
    uint32_t VirtualSize;
    uint32_t VirtualAddress;
    uint32_t SizeOfRawData;
    uint32_t PointerToRawData;
    uint32_t PointerToRelocations;
    uint32_t PointerToLinenumbers;
    uint16_t NumberOfRelocations;
    uint16_t NumberOfLinenumbers;
    uint32_t Characteristics;
};
#if defined _MSC_VER
#pragma pack(pop)
#endif

/**
 * @brief This structure stores data of PE header section in PE file
 */
#if defined _MSC_VER
#pragma pack(push, 1)
#endif
struct
#if defined __clang__ || __GNUC__
    __attribute__((packed))
#endif
    PEFile
{
    /// @name File headers
    ///@{

    /// File magic
    uint32_t magic;
    /// Main header
    struct PEHeader header;
    /// List of section headers with the size of header.number_of_sections
    struct SectionHeader *sectionHeaders;
    ///@}
};
#if defined _MSC_VER
#pragma pack(pop)
#endif

void readFile(FILE *in, struct PEFile *peFile);
void printSection(FILE *in, FILE *out, struct PEFile *peFile, char *section);
